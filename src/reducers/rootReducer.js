import {FETCH_QUOTE} from '../actions/quoteActions';


const initState = {
  text:  "here is a quote",
  author: "me"
}


const rootReducer = (state = initState, action)=>{
  switch (action.type) {
    case FETCH_QUOTE:
      console.log('FETCH_STUFF Action')
      return action.quote;
    default:
      return state;
 }
}

export default rootReducer
