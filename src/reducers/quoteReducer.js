
import {FETCH_QUOTE, RECEIVE_QUOTE} from '../actions/quoteActions';

export const  quote = (state =[], action)=> {
  let newState;
  switch (action.type) {
    case FETCH_QUOTE:
      console.log('FETCH_STUFF Action')
      return action.quote;
    case RECEIVE_QUOTE:
      newState = action.quote;
      console.log('RECEIVE_STUFF Action')
      return newState;
    default:
      return state;
  }
}
