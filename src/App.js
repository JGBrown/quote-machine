import React from 'react';
import { connect } from 'react-redux';
import { fetchQuote} from './actions/quoteActions'
import './App.css';



class App extends React.Component {

componentDidMount(){

}

render() {
  // set up twitter post url for sharing quote
    const author = this.props.author;
    const text = `"${this.props.text} "`;
    const tweeturl = `https://twitter.com/intent/tweet?hashtags=quotes&related=freecodecamp&text=${text} ${author}`;
    const res = encodeURI(tweeturl)
    console.log(res);

    const getquote = () =>{fetch("https://type.fit/api/quotes")
       .then(function(response) {
         return response.json();
       })
       .then(function(data) {
         let randomdata = data[Math.floor(Math.random() * data.length)]
         return handleUpdateQuote(randomdata)
       }
     );}

    const handleUpdateQuote = (newquote) =>{
        this.props.updateQuote(newquote)
      }

  return (

      <div className="App" ref={el => (this.div = el)}>
        <div id="quote-box">
          <div id="text">" {this.props.text} "</div>
          <div id="author">- {this.props.author} </div>
          <div id="buttons">
            <a href={res} id="tweet-quote" target="_blank">
              <img className="button" src="./TwitterLogo.png" alt="Tweet it" ></img>
              </a>
            <button className="button" onClick={getquote} id="new-quote">New Quote</button>
          </div>
        </div>
        <div className="vanity"> by JGBrown</div>

      </div>
      )
  };
}

  const mapStateToProps = (state) => {
      return {
        text: state.text,
        author: state.author
    }
  }

  const mapDispatchToProps = (dispatch) => {
    return {
        updateQuote: (quote) => { dispatch(fetchQuote(quote))}
      }

  }



export default connect(mapStateToProps,mapDispatchToProps)(App);
